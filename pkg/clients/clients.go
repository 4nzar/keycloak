package kc

import (
	"log"

	kc_auth "gitlab.com/4nzar/keycloak/pkg/auth"
	kc_groups "gitlab.com/4nzar/keycloak/pkg/groups"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
	kc_users "gitlab.com/4nzar/keycloak/pkg/users"
)

type Realm struct {
	Name   string `json:"name"`
	Client string `json:"client"`
}

type Client struct {
	kc_auth.AuthResource
	Users  *kc_users.UsersResource
	Groups *kc_groups.GroupsResource
}

func NewClient(username, password *string) *Client {
	// Create a new Keycloak client using the given username and password
	// and the default realm
	auth := &kc_auth.AuthResource{
		Username: username,
		Password: password,
	}
	log.Printf("Creating new client with username %s\n", *username)
	client := &Client{
		kc_auth.AuthResource{
			Username: username,
			Password: password,
		},
		&kc_users.UsersResource{Auth: auth},
		&kc_groups.GroupsResource{Auth: auth},
	}
	response := client.DoAuth()
	if response.Error != nil {
		log.Printf("An error occured during authentication : %v\n", response.Error)
		return nil
	}
	log.Printf("Successfully authenticated as %s\n", *username)
	return client
}

func NewClientWithToken(token *kc_model.AuthToken) *Client {
	auth := &kc_auth.AuthResource{
		Token: token,
	}
	client := &Client{
		kc_auth.AuthResource{Token: token},
		&kc_users.UsersResource{Auth: auth},
		&kc_groups.GroupsResource{Auth: auth},
	}
	log.Printf("NewClientWithToken (response): %+v\n", client)
	return client
}

func NewClientWithAccessToken(access_token string) *Client {
	token := &kc_model.AuthToken{AccessToken: access_token}
	return NewClientWithToken(token)
}
