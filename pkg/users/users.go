package kc_users

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"

	"github.com/google/go-querystring/query"
	"github.com/google/uuid"
	goose_http "gitlab.com/4nzar/goose/pkg/http"
	goose_slice "gitlab.com/4nzar/goose/pkg/slice"
	"gitlab.com/4nzar/keycloak/internal/utils"
	kc_auth "gitlab.com/4nzar/keycloak/pkg/auth"
	kc_groups "gitlab.com/4nzar/keycloak/pkg/groups"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

type UsersResource struct {
	Auth *kc_auth.AuthResource
}

func (instance *UsersResource) Create(request *kc_model.UserRepresentation) *kc_model.APIResponse[*kc_model.UserRepresentation] {
	path := utils.GetFmtURL(true, "users")
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	log.Printf("Creating user: %v\n", request.Username)
	queries := url.Values{}
	response, err := goose_http.Post(path, headers, queries, request)
	if err != nil {
		log.Printf("Failed to create user: %s\n", err)
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		log.Printf("Error reading response body: %s\n", err.Error())
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		log.Printf("Failed to create user: %s\n", string(data))
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	options := &kc_model.GetUserListOptions{Email: *request.Email}
	response_users := instance.GetList(options)
	if response_users.Error != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response_users.StatusCode,
			Error:      response_users.Error,
		}
	}
	if len(response_users.Data) == 0 {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusNotFound,
			Error: fmt.Errorf("couldn't find user '%s'", *request.Email),
		}
	}
	log.Printf("Successfully created user: %+v\n", *response_users.Data[0])
	return &kc_model.APIResponse[*kc_model.UserRepresentation]{
		StatusCode: http.StatusOK,
		Data:       response_users.Data[0],
	}
}

func (instance *UsersResource) GetInformation(id *uuid.UUID) *kc_model.APIResponse[*kc_model.UserRepresentation] {
	path := utils.GetFmtURL(true, fmt.Sprintf("users/%s", *id))
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{StatusCode: http.StatusInternalServerError, Error: err}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	user := &kc_model.UserRepresentation{}
	if err := json.Unmarshal(data, &user); err != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(string(data)),
		}
	}
	response_groups := instance.GetGroups(user.ID)
	if response_groups.Error != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response_groups.StatusCode,
			Error:      response_groups.Error,
		}
	}
	response_credentials := instance.GetCredentials(user.ID)
	if response_credentials.Error != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response_credentials.StatusCode,
			Error:      response_credentials.Error,
		}
	}
	user.Credentials = response_credentials.Data
	user.Groups = goose_slice.Map(response_groups.Data, func(role *kc_model.GroupRepresentation) string { return *role.Name })
	return &kc_model.APIResponse[*kc_model.UserRepresentation]{
		StatusCode: response.StatusCode,
		Data:       user,
	}
}

func (instance *UsersResource) Update(id *uuid.UUID, request *kc_model.UserRepresentation) *kc_model.APIResponse[*kc_model.UserRepresentation] {
	path := utils.GetFmtURL(true, fmt.Sprintf("users/%s", *id))
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Put(path, headers, queries, request)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	response_user := instance.GetInformation(id)
	if response_user.Error != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response_user.StatusCode,
			Error:      response_user.Error,
		}
	}
	return &kc_model.APIResponse[*kc_model.UserRepresentation]{
		StatusCode: http.StatusOK,
		Data:       response_user.Data,
	}
}

func (instance *UsersResource) Delete(id *uuid.UUID) *kc_model.APIResponse[*kc_model.UserRepresentation] {
	response_user := instance.GetInformation(id)
	if response_user.Error != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response_user.StatusCode,
			Error:      response_user.Error,
		}
	}
	path := utils.GetFmtURL(true, fmt.Sprintf("users/%s", id))
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Delete(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	return &kc_model.APIResponse[*kc_model.UserRepresentation]{
		StatusCode: http.StatusOK,
		Data:       response_user.Data,
	}
}

func (instance *UsersResource) GetList(options *kc_model.GetUserListOptions) *kc_model.APIResponse[[]*kc_model.UserRepresentation] {
	path := utils.GetFmtURL(true, "users")
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries, _ := query.Values(options)
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[[]*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return &kc_model.APIResponse[[]*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[[]*kc_model.UserRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	var users []*kc_model.UserRepresentation
	if err := json.Unmarshal(data, &users); err != nil {
		return &kc_model.APIResponse[[]*kc_model.UserRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	result := goose_slice.Map(users, func(user *kc_model.UserRepresentation) *kc_model.UserRepresentation {
		response_groups := instance.GetGroups(user.ID)
		if response_groups.Error != nil {
			return nil
		}
		response_credentials := instance.GetCredentials(user.ID)
		if response_credentials.Error != nil {
			return nil
		}
		user.Credentials = response_credentials.Data
		user.Groups = goose_slice.Map(response_groups.Data, func(role *kc_model.GroupRepresentation) string { return *role.Name })
		return user
	})
	return &kc_model.APIResponse[[]*kc_model.UserRepresentation]{
		StatusCode: http.StatusOK,
		Data:       result,
	}
}

func (instance *UsersResource) Enable(id *uuid.UUID, enabled bool) *kc_model.APIResponse[*kc_model.UserRepresentation] {
	user := &kc_model.UserRepresentation{
		Enabled: &enabled,
	}
	response_update := instance.Update(id, user)
	if response_update.Error != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response_update.StatusCode,
			Error:      response_update.Error,
		}
	}
	response_user := instance.GetInformation(id)
	if response_user.Error != nil {
		return &kc_model.APIResponse[*kc_model.UserRepresentation]{
			StatusCode: response_user.StatusCode,
			Error:      response_user.Error,
		}
	}
	return &kc_model.APIResponse[*kc_model.UserRepresentation]{
		StatusCode: http.StatusOK,
		Data:       response_user.Data,
	}
}

func (instance *UsersResource) JoinGroup(id *uuid.UUID, group string) *kc_model.APIResponse[*kc_model.GroupRepresentation] {
	groups_resource := kc_groups.GroupsResource{Auth: instance.Auth}
	response_groups := groups_resource.GetList()
	if response_groups.Error != nil {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: response_groups.StatusCode,
			Error:      response_groups.Error,
		}
	}
	filtered := goose_slice.Filter(response_groups.Data, func(grp *kc_model.GroupRepresentation) bool {
		return *grp.Name == group
	})
	if len(filtered) == 0 {
		return nil
	}
	group_id := *filtered[0].ID
	log.Printf("PUT user groups\n- user_id : %+v\n- group_id : %+v\n", *id, group_id)
	path := utils.GetFmtURL(true, fmt.Sprintf("users/%s/groups/%s", *id, group_id))
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	body := map[string]interface{}{}
	response, err := goose_http.Put(path, headers, queries, body)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
		StatusCode: http.StatusOK,
		Data:       nil,
	}
}

func (instance *UsersResource) LeaveGroup(id *uuid.UUID, group string) *kc_model.APIResponse[*kc_model.GroupRepresentation] {
	response_groups := instance.GetGroups(id)
	if response_groups.Error != nil {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: response_groups.StatusCode,
			Error:      response_groups.Error,
		}
	}
	filtered := goose_slice.Filter(response_groups.Data, func(grp *kc_model.GroupRepresentation) bool {
		return *grp.Name == group
	})
	if len(filtered) == 0 {
		return nil
	}
	group_id := *filtered[0].ID
	path := utils.GetFmtURL(true, fmt.Sprintf("users/%s/groups/%s", *id, group_id))
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Delete(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	return &kc_model.APIResponse[*kc_model.GroupRepresentation]{
		StatusCode: http.StatusOK,
		Data:       nil,
	}
}

func (instance *UsersResource) GetGroups(id *uuid.UUID) *kc_model.APIResponse[[]*kc_model.GroupRepresentation] {
	path := utils.GetFmtURL(true, fmt.Sprintf("users/%s/groups", *id))
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	groups := []*kc_model.GroupRepresentation{}
	if len(data) > 0 {
		if err := json.Unmarshal(data, &groups); err != nil {
			return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
				StatusCode: http.StatusInternalServerError,
				Error:      fmt.Errorf(string(data)),
			}
		}
	}
	return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
		StatusCode: http.StatusOK,
		Data:       groups,
	}
}

func (instance *UsersResource) GetCredentials(id *uuid.UUID) *kc_model.APIResponse[[]*kc_model.CredentialRepresentation] {
	path := utils.GetFmtURL(true, fmt.Sprintf("users/%s/credentials", *id))
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[[]*kc_model.CredentialRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return &kc_model.APIResponse[[]*kc_model.CredentialRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[[]*kc_model.CredentialRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	var credentials []*kc_model.CredentialRepresentation
	if err := json.Unmarshal(data, &credentials); err != nil {
		return &kc_model.APIResponse[[]*kc_model.CredentialRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(string(data)),
		}
	}
	return &kc_model.APIResponse[[]*kc_model.CredentialRepresentation]{
		StatusCode: http.StatusOK,
		Data:       credentials,
	}
}


