package kc_model

type GetUserListOptions struct {
	BriefRepresentation bool   `url:"briefRepresentation,omitempty"`
	Email               string `url:"email,omitempty"`
	EmailVerified       bool   `url:"emailVerified,omitempty"`
	Enabled             bool   `url:"enabled,omitempty"`
	Exact               bool   `url:"exact,omitempty"`
	First               int64  `url:"first,omitempty"`
	Max                 int64  `url:"max,omitempty"`
	Firstname           string `url:"firstName,omitempty"`
	Lastname            string `url:"lastName,omitempty"`
	IdpAlias            string `url:"idpAlias,omitempty"`
	IdpUserID           string `url:"idpUserId,omitempty"`
	Q                   string `url:"q,omitempty"`
	Search              string `url:"search,omitempty"`
	Username            string `url:"username,omitempty"`
}
