package kc_model

import "github.com/google/uuid"

type GroupRepresentation struct {
	ID        *uuid.UUID            `json:"id,omitempty"`
	Name      *string               `json:"name,omitempty"`
	Path      *string               `json:"path,omitempty"`
	SubGroups []GroupRepresentation `json:"subGroups,omitempty"`
}
