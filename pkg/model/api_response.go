package kc_model

type APIResponse[T any] struct {
	StatusCode int
	Data T
	Error error
}
