package kc_auth

import (
	"net/url"
	"time"

	"gitlab.com/4nzar/keycloak/internal/auth"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func GetAccessToken(auth_info kc_model.AuthInfo) (*string, error) {
	var token kc_model.AuthToken
	now := time.Now()
	var err error
	var form url.Values
	if now.After(token.ExpiresDate) && now.Before(token.RefreshExpiresDate) {
		form = auth.PrepareRequest(auth_info, &token.RefreshToken)
	} else if now.After(token.RefreshExpiresDate) {
		form = auth.PrepareRequest(auth_info, nil)
	}
	token, err = auth.DoAuthRequest(form, token)
	if err != nil {
		return nil, err
	}
	return &token.AccessToken, err
}

func GetToken(auth_info kc_model.AuthInfo) (*kc_model.AuthToken, error) {
	var token kc_model.AuthToken
	now := time.Now()
	var err error
	var form url.Values
	if now.After(token.ExpiresDate) && now.Before(token.RefreshExpiresDate) {
		form = auth.PrepareRequest(auth_info, &token.RefreshToken)
	} else if now.After(token.RefreshExpiresDate) {
		form = auth.PrepareRequest(auth_info, nil)
	}
	token, err = auth.DoAuthRequest(form, token)
	if err != nil {
		return nil, err
	}
	return &token, err
}
