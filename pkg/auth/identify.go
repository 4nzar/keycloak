package kc_auth

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	goose_http "gitlab.com/4nzar/goose/pkg/http"
	"gitlab.com/4nzar/keycloak/internal/utils"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func Identify(token string) (*kc_model.UserInformation, error) {
	path := utils.GetFmtURL(false, "protocol/openid-connect/userinfo")
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", token),
	}
	queries := url.Values{}
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		return nil, err
	}
	data, err := io.ReadAll(response.Body)
	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(string(data))
	}
	var user *kc_model.UserInformation
	if err := json.Unmarshal(data, &user); err != nil {
		return nil, err
	}
	return user, nil
}
