package kc_auth

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"time"

	goose_http "gitlab.com/4nzar/goose/pkg/http"
	"gitlab.com/4nzar/keycloak/internal/auth"
	"gitlab.com/4nzar/keycloak/internal/utils"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

type AuthResource struct {
	Username *string
	Password *string
	Token    *kc_model.AuthToken
}

func (instance *AuthResource) GetAccessToken() (*string, error) {
	response := instance.DoAuth()
	if response.Error != nil {
		return nil, response.Error
	}
	return &response.Data.AccessToken, nil
}

func (instance *AuthResource) DoAuth() *kc_model.APIResponse[*kc_model.AuthToken] {
	if !instance.CanBeAuthenticated() {
		return &kc_model.APIResponse[*kc_model.AuthToken]{
			StatusCode: http.StatusUnauthorized,
			Error:      fmt.Errorf("Unauthorized"),
		}
	}
	var token kc_model.AuthToken
	now := time.Now()
	var err error
	var form url.Values
	if now.After(token.ExpiresDate) && now.Before(token.RefreshExpiresDate) {
		form = auth.PrepareForm(*instance.Username, *instance.Password, &token.RefreshToken)
	} else if now.After(token.RefreshExpiresDate) {
		form = auth.PrepareForm(*instance.Username, *instance.Password, nil)
	}
	token, err = auth.DoAuthRequest(form, token)
	if err != nil {
		log.Printf("err : %v\n", err)
		return &kc_model.APIResponse[*kc_model.AuthToken]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	instance.Token = &token
	return &kc_model.APIResponse[*kc_model.AuthToken]{
		StatusCode: http.StatusOK,
		Data:       &token,
	}
}

func (instance *AuthResource) Identify() *kc_model.APIResponse[*kc_model.UserInformation] {
	if !instance.CanBeAuthenticated() {
		return &kc_model.APIResponse[*kc_model.UserInformation]{
			StatusCode: http.StatusUnauthorized,
			Error:      fmt.Errorf("Unauthorized"),
		}
	}
	path := utils.GetFmtURL(false, "protocol/openid-connect/userinfo")
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		log.Printf("Identify on Get (error): %v\n", err)
		return &kc_model.APIResponse[*kc_model.UserInformation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		log.Printf("Identify on ReadAll (error): %v\n", err)
		return &kc_model.APIResponse[*kc_model.UserInformation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	if response.StatusCode != http.StatusOK {
		log.Printf("no StatusOK (error): %v\n", response.StatusCode)
		log.Printf("data : %v\n", string(data))
		return &kc_model.APIResponse[*kc_model.UserInformation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	var user *kc_model.UserInformation
	if err := json.Unmarshal(data, &user); err != nil {
		log.Printf("Identify on Unmarshal (error): %v\n", err)
		return &kc_model.APIResponse[*kc_model.UserInformation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	log.Printf("Identify (response): %v\n", user)
	return &kc_model.APIResponse[*kc_model.UserInformation]{
		StatusCode: http.StatusOK,
		Data:       user,
	}
}

func (instance *AuthResource) CanBeAuthenticated() bool {
	if (instance.Username != nil && instance.Password != nil) || instance.Token != nil {
		return true
	}
	return false
}
