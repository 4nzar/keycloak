package kc_md

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func IsAuthenticated() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ctx.GetHeader("Authorization")
		if !strings.HasPrefix(token, "Bearer ") {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, "User not authorized")
			return
		}
		response_info := kc.NewClientWithAccessToken(token).Identify()
		if response_info.Error != nil {
			ctx.AbortWithStatusJSON(response_info.StatusCode, response_info.Error)
			return
		}
		ctx.Next()
	}
}

func IsAuthenticatedWithMapContext(mapper func(*gin.Context, string, *kc_model.UserInformation)) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ctx.GetHeader("Authorization")
		if !strings.HasPrefix(token, "Bearer ") {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, "User not authorized")
			return
		}
		response_info := kc.NewClientWithAccessToken(token).Identify()
		if response_info.Error != nil {
			ctx.AbortWithStatusJSON(response_info.StatusCode, response_info.Error)
			return
		}
		mapper(ctx, token, response_info.Data)
		ctx.Next()
	}
}
