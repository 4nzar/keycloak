package kc_md

import (
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	goose_slice "gitlab.com/4nzar/goose/pkg/slice"
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

type Location int

const (
	IN_HEADER Location = iota
	IN_QUERY
)

func AllowGroups(tokenLocation Location, expected_groups []string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var request_token string
		var token string
		if tokenLocation == IN_HEADER {
			request_token = ctx.GetHeader("Authorization")
			if !strings.HasPrefix(request_token, "Bearer ") {
				ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
				return
			}
			split_token := strings.Split(request_token, "Bearer ")
			token = split_token[1]
		}
		if tokenLocation == IN_QUERY {
			var found bool
			request_token, found = ctx.GetQuery("token")
			if !found {
				ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
				return
			}
			token = request_token
		}
		log.Printf("token  : %v\n", token)
		response_info := kc.NewClientWithAccessToken(token).Identify()
		if response_info.Error != nil {
			ctx.AbortWithStatusJSON(response_info.StatusCode, response_info.Error)
			return
		}
		log.Printf("user identification : %v\n", response_info.Data)
		response_groups := kc.NewClientWithAccessToken(token).Users.GetGroups(response_info.Data.ID)
		if response_groups.Error != nil {
			ctx.AbortWithStatusJSON(response_groups.StatusCode, response_groups.Error)
			return
		}
		log.Printf("groups found : %v\n", response_groups.Data)
		groups := goose_slice.Map(response_groups.Data, func(role *kc_model.GroupRepresentation) string {
			return *role.Name
		})
		log.Printf("mapped groups found : %v\n", groups)
		if !goose_slice.ContainsAtLeast(groups, expected_groups) {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
			return
		}
		ctx.Next()
	}
}

func AllowGroupsMapContext(tokenLocation Location, expected_groups []string, mapper func(*gin.Context, string, *kc_model.UserInformation, []*kc_model.GroupRepresentation)) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var request_token string
		var token string
		if tokenLocation == IN_HEADER {
			request_token = ctx.GetHeader("Authorization")
			if !strings.HasPrefix(request_token, "Bearer ") {
				ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
				return
			}
			split_token := strings.Split(request_token, "Bearer ")
			token = split_token[1]
		}
		if tokenLocation == IN_QUERY {
			var found bool
			request_token, found = ctx.GetQuery("token")
			if !found {
				ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
				return
			}
			token = request_token
		}
		log.Printf("token  : %v\n", token)
		response_info := kc.NewClientWithAccessToken(token).Identify()
		if response_info.Error != nil {
			log.Printf("response_info  : %+v\n", response_info)
			ctx.AbortWithStatusJSON(response_info.StatusCode, response_info.Error)
			return
		}
		log.Printf("user identification : %v\n", response_info.Data)
		response_groups := kc.NewClientWithAccessToken(token).Users.GetGroups(response_info.Data.ID)
		if response_groups.Error != nil {
			log.Printf("response_groups  : %+v\n", response_groups)
			ctx.AbortWithStatusJSON(response_groups.StatusCode, response_groups.Error)
			return
		}
		log.Printf("groups found : %v\n", response_groups.Data)
		groups := goose_slice.Map(response_groups.Data, func(role *kc_model.GroupRepresentation) string {
			return *role.Name
		})
		log.Printf("mapped groups found : %v\n", groups)
		if !goose_slice.ContainsAtLeast(groups, expected_groups) {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, "User not assigned to any group")
			return
		}
		mapper(ctx, token, response_info.Data, response_groups.Data)
		ctx.Next()
	}
}
