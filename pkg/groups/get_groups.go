package kc_groups

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	goose_http "gitlab.com/4nzar/goose/pkg/http"
	"gitlab.com/4nzar/keycloak/internal/utils"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func GetGroups(token string) *kc_model.APIResponse[[]*kc_model.GroupRepresentation] {
	path := utils.GetFmtURL(true, "groups")
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", token),
	}
	queries := url.Values{}
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	var result []*kc_model.GroupRepresentation
	if err := json.Unmarshal(data, &result); err != nil {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(string(data)),
		}
	}
	return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
		StatusCode: http.StatusOK,
		Data:       result,
	}
}
