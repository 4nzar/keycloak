package kc_groups

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	goose_http "gitlab.com/4nzar/goose/pkg/http"
	"gitlab.com/4nzar/keycloak/internal/utils"
	kc_auth "gitlab.com/4nzar/keycloak/pkg/auth"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

type GroupsResource struct {
	Auth *kc_auth.AuthResource
}

func (instance *GroupsResource) GetList() *kc_model.APIResponse[[]*kc_model.GroupRepresentation] {
	path := utils.GetFmtURL(true, "groups")
	headers := map[string]string{
		"Content-Type":  "application/json; charset=UTF-8",
		"Authorization": fmt.Sprintf("Bearer %s", instance.Auth.Token.AccessToken),
	}
	queries := url.Values{}
	response, err := goose_http.Get(path, headers, queries)
	if err != nil {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      err,
		}
	}
	data, err := io.ReadAll(response.Body)
	if response.StatusCode >= http.StatusBadRequest {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: response.StatusCode,
			Error:      fmt.Errorf(string(data)),
		}
	}
	var groups []*kc_model.GroupRepresentation
	if err := json.Unmarshal(data, &groups); err != nil {
		return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(string(data)),
		}
	}
	return &kc_model.APIResponse[[]*kc_model.GroupRepresentation]{
		StatusCode: http.StatusOK,
		Data:       groups,
	}
}
