# KeyCloak - Go client

# Environment

`KC_URL`: 

`KC_OLD_VERSION`:

`KC_PLATFORM_REALM`:

`KC_PLATFORM_CLIENT_ID`:

`KC_PLATFORM_CLIENT_SECRET`:

# Clients

NewClient:
```go
func NewClient(username, password *string) *Client
```

*example*:
```go
import (
    ...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }
    // ...
}

```

NewClientWithToken:
```go
func NewClientWithToken(token *model.AuthToken) *Client
```

*example*:
```go
import (
    // ...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    token := kc_model.AuthToken{}
    client := kc.NewClientWithToken(token)
    // ...
}

```

NewClientWithAccessToken:
```go
func NewClientWithAccessToken(access_token string) *Client
```

*example*:
```go
import (
    // ...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
)

func main() {
    // ...
    access_token := eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiZGVtbyIsIklzc3VlciI6Iklzc3... // token generated via keycloak
    client := kc.NewClientWithAccessToken(token)
    // ...
}

```


# Resources

## Users

Create a user:
```go
func (instance *UsersResource) Create(request *model.UserRepresentation) error
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }
    new_user := &kc_model.UserRepresentation{} // see UserRepresentation model
    if err := client.Users.Create(new_user); err != nil {
        fmt.Printf("an error occured during the creation of a new user - %v\n", err.Error())
    }
    // ...
}

```

Create a user in bulk:
```go
func (instance *UsersResource) CreateInBulk(request []*model.UserRepresentation) error
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }
    list_new_user := []*model.UserRepresentation{...}
    if err := client.Users.CreateInBulk(list_new_user); err != nil {
        fmt.Printf("an error occured during the creation of a new user - %v\n", err.Error())
    }
    // ...
}

```

Get user information:
```go
func (instance *UsersResource) GetInformation(id *uuid.UUID) (*model.UserRepresentation, error)
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    user_info, err := client.Users.GetInformation(&id)
    if err != nil {
        fmt.Printf("an error occured when fetching user information - %v\n", err.Error())
    }
    // ...
}

```

Get list of user:
```go
func (instance *UsersResource) GetList(options *model.GetUserListOptions) ([]*model.UserRepresentation, error)
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    options := &kc_model.GetUserListOptions{}
    users, err := client.Users.GetList(options)
    if err != nil {
        fmt.Printf("an error occured when fetching user information - %v\n", err.Error())
    }
    // ...
}

```

Delete a user:
```go
func (instance *UsersResource) Delete(id *uuid.UUID) error
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    if err := client.Users.Delete(&id); err != nil {
        fmt.Printf("an error occured during deletion - %v\n", err.Error())
    }
    // ...
}

```

Update a user:
```go
func (instance *UsersResource) Update(id *uuid.UUID, request *model.UserRepresentation) error
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    edit := &kc_model.UserRepresentation{} // see UserRepresentation model
    if err := client.Users.Update(&id, edit); err != nil {
        fmt.Printf("an error occured during the update - %v\n", err.Error())
    }
    // ...
}

```

Change user status:
```go
func (instance *UsersResource) Enable(id *uuid.UUID, enabled bool) error
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    enable := false
    if err := client.Users.Enable(&id, enable); err != nil {
        fmt.Printf("an error occured when changing status - %v\n", err.Error())
    }
    // ...
}

```

Join group:
```go
func (instance *UsersResource) JoinGroup(id *uuid.UUID, group string) error
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    group := "new_group"
    if err := client.Users.JoinGroup(&id, group); err != nil {
        fmt.Printf("an error occured when joining group %v - %v\n", group, err.Error())
    }
    // ...
}

```

Leave group:
```go
func (instance *UsersResource) LeaveGroup(id *uuid.UUID, group string) error
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    group := "old_group"
    if err := client.Users.LeaveGroup(&id, group); err != nil {
        fmt.Printf("an error occured when joining group %v - %v\n", group, err.Error())
    }
    // ...
}

```

Get list of groups that user joined:
```go
func (instance *UsersResource) GetGroups(id *uuid.UUID) ([]*model.GroupRepresentation, error)
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    groups, err := client.Users.GetGroups(&id)
    if err != nil {
        fmt.Printf("an error occured when fecthing the list of groups joined by the user %v - %v\n", id, err.Error())
    }
    // ...
}

```

Get list of user credentials:
```go
func (instance *UsersResource) GetCredentials(id *uuid.UUID) ([]*model.CredentialRepresentation, error)
```

*example*:
```go
import (
    //...
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

func main() {
    // ...
    client := kc.NewClient(username, password)
    if client == nil {
        fmt.Println("Error: something is wrong!")
        os.Exit(1)
    }

    id := "585f6ddc-a555-4755-be0d-ecbe7aa43966"
    credentials, err := client.Users.GetCredentials(&id)
    if err != nil {
        fmt.Printf("an error occured when fecthing the list of user credentials - %v\n", err.Error())
    }
    // ...
}

```







### Roles

### Groups

## Model

```go
type AuthInfo struct {
	Username string
	Password string
}
```
```go
type AuthToken struct {
	AccessToken        string `json:"access_token"`
	ExpiresIn          int64  `json:"expires_in"`
	RefreshExpiresIn   int64  `json:"refresh_expires_in"`
	RefreshToken       string `json:"refresh_token"`
	TokenType          string `json:"token_type"`
	NotBeforePolicy    int64  `json:"not-before-policy"`
	SessionState       string `json:"session_state"`
	Scope              string `json:"scope"`
	ExpiresDate        time.Time
	RefreshExpiresDate time.Time
}
```
```go
type CredentialRepresentation struct {
	CreatedDate    int64  `json:"createdDate,omitempty"`
	CredentialData string `json:"credentialData,omitempty"`
	ID             string `json:"id,omitempty"`
	Priority       int32  `json:"priority,omitempty"`
	SecretData     string `json:"secretData,omitempty"`
	Temporary      bool   `json:"temporary,omitempty"`
	Type           string `json:"type,omitempty"`
	UserLabel      string `json:"userLabel,omitempty"`
	Value          string `json:"value,omitempty"`
}
```
```go
type UserConsentRepresentation struct {
	ClientID            string   `json:"clientId,omitempty"`
	CreatedDate         int64    `json:"createdDate,omitempty"`
	GrantedClientScopes []string `json:"grantedClientScopes,omitempty"`
	LastUpdatedDate     int64    `json:"lastUpdatedDate,omitempty"`
}
```
```go
type UserCreateRequest struct{}
```
```go
type UserInformation struct {
	ID                uuid.UUID `json:"sub"`
	EmailVerified     bool   `json:"email_verified"`
	Name              string `json:"name"`
	PreferredUsername string `json:"preferred_username"`
	GivenName         string `json:"given_name"`
	FamilyName        string `json:"family_name"`
	Email             string `json:"email"`
}
```
```go
type UserRepresentation struct {
	Access                     map[string]bool             `json:"access,omitempty"`
	Attributes                 map[string]string           `json:"attributes,omitempty"`
	ClientConsents             []UserConsentRepresentation `json:"clientConsents,omitempty"`
	ClientRoles                map[string]string           `json:"clientRoles,omitempty"`
	CreatedTimestamp           int64                       `json:"createdTimestamp,omitempty"`
	Credentials                []CredentialRepresentation  `json:"credentials,omitempty"`
	DisableableCredentialTypes []string                    `json:"disableableCredentialTypes,omitempty"`
	Email                      string                      `json:"email,omitempty"`
	EmailVerified              bool                        `json:"emailVerified,omitempty"`
	Enabled                    bool                        `json:"enabled,omitempty"`
	FederatedIdentities        []string                    `json:"federatedIdentities,omitempty"`
	FederationLink             string                      `json:"federationLink,omitempty"`
	FirstName                  string                      `json:"firstName,omitempty"`
	Groups                     []string                    `json:"groups,omitempty"`
	ID                         uuid.UUID                   `json:"id,omitempty"`
	LastName                   string                      `json:"lastName,omitempty"`
	NotBefore                  int                         `json:"notBefore,omitempty"`
	Origin                     string                      `json:"origin,omitempty"`
	RealmRoles                 []string                    `json:"realmRoles,omitempty"`
	RequiredActions            []string                    `json:"requiredActions,omitempty"`
	Self                       string                      `json:"self,omitempty"`
	ServiceAccountClientID     string                      `json:"serviceAccountClientId,omitempty"`
	Username                   string                      `json:"username,omitempty"`
}
```
```go
type UserRole struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name"`
	Path string    `json:"path"`
}
```
```go
type UserUpdateRequest struct{}

```
