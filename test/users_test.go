package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	kc_model "gitlab.com/4nzar/keycloak/pkg/model"
)

/*
func TestUserResourceCreate(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			"/admin/realms/demo/users",
			fmt.Sprintf("/admin/realms/demo/users/%s", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s", id) {
			if req.Method == "PUT" {
				req_body, err := ioutil.ReadAll(req.Body)
				if err != nil {
					t.Fatal(err)
				}
				var data *kc_model.UserRepresentation
				err = json.Unmarshal(req_body, &data)
				assert.Nil(t, err)

				assert.NotNil(t, data)
				assert.Equal(t, "jdoe@demo.com", *data.Username)
				assert.Equal(t, "John", *data.FirstName)
				assert.Equal(t, "DOE", *data.LastName)
				rw.Write([]byte{})
				return
			}
			if req.Method == "GET" {
				username := "jdoe@demo.com"
				firstname := "John"
				lastname := "DOE"
				user := &kc_model.UserRepresentation{
					ID:        &id,
					Username:  &username,
					FirstName: &firstname,
					LastName:  &lastname,
				}
				response, err := json.Marshal(user)
				assert.Nil(t, err)
				rw.Write(response)
				return
			}
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			assert.Equal(t, "GET", req.Method)
			name := "demo"
			path := "/demo"
			g1 := &kc_model.GroupRepresentation{
				ID:        &id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id) {
			assert.Equal(t, "GET", req.Method)
			c1 := &kc_model.CredentialRepresentation{
				ID:    id.String(),
				Type:  "password",
				Value: "1234",
			}
			groups := []*kc_model.CredentialRepresentation{c1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		t.Fail()
		assert.Equal(t, req.URL.String(), "/admin/realms/demo/users")
		req_body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			t.Fatal(err)
		}
		var data *kc_model.UserRepresentation
		err = json.Unmarshal(req_body, &data)
		assert.Nil(t, err)

		assert.NotNil(t, data)
		assert.Equal(t, "jdoe@demo.com", *data.Username)
		assert.Equal(t, "John", *data.FirstName)
		assert.Equal(t, "DOE", *data.LastName)
		rw.Write([]byte{})
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	name := "John"
	family_name := "DOE"
	email := "jdoe@demo.com"
	new_user := &kc_model.UserRepresentation{
		Username:  &email,
		FirstName: &name,
		LastName:  &family_name,
	}
	client := kc.NewClientWithAccessToken(token)
	got := client.Users.Create(new_user)
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Error)
}
*/

func TestUserResourceUpdate(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			fmt.Sprintf("/admin/realms/demo/users/%s", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s", id) {
			if req.Method == "PUT" {
				req_body, err := ioutil.ReadAll(req.Body)
				if err != nil {
					t.Fatal(err)
				}
				var data *kc_model.UserRepresentation
				err = json.Unmarshal(req_body, &data)
				assert.Nil(t, err)

				assert.NotNil(t, data)
				assert.Equal(t, "jdoe@demo.com", *data.Username)
				assert.Equal(t, "John", *data.FirstName)
				assert.Equal(t, "DOE", *data.LastName)
				rw.Write([]byte{})
				return
			}
			if req.Method == "GET" {
				username := "jdoe@demo.com"
				firstname := "John"
				lastname := "DOE"
				user := &kc_model.UserRepresentation{
					ID:        &id,
					Username:  &username,
					FirstName: &firstname,
					LastName:  &lastname,
				}
				response, err := json.Marshal(user)
				assert.Nil(t, err)
				rw.Write(response)
				return
			}
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			assert.Equal(t, "GET", req.Method)
			name := "demo"
			path := "/demo"
			g1 := &kc_model.GroupRepresentation{
				ID:        &id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id) {
			assert.Equal(t, "GET", req.Method)
			c1 := &kc_model.CredentialRepresentation{
				ID:    id.String(),
				Type:  "password",
				Value: "1234",
			}
			groups := []*kc_model.CredentialRepresentation{c1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		t.Fail()
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	name := "John"
	family_name := "DOE"
	email := "jdoe@demo.com"
	new_user := &kc_model.UserRepresentation{
		Username:  &email,
		FirstName: &name,
		LastName:  &family_name,
	}
	got := kc.NewClientWithAccessToken(token).Users.Update(&id, new_user)
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Data)
}

func TestUserResourceDelete(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			fmt.Sprintf("/admin/realms/demo/users/%s", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s", id) {
			if req.Method == "DELETE" {
				rw.Write([]byte{})
				return
			}
			if req.Method == "GET" {
				username := "jdoe@demo.com"
				firstname := "John"
				lastname := "DOE"
				user := &kc_model.UserRepresentation{
					ID:        &id,
					Username:  &username,
					FirstName: &firstname,
					LastName:  &lastname,
				}
				response, err := json.Marshal(user)
				assert.Nil(t, err)
				rw.Write(response)
				return
			}
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			assert.Equal(t, "GET", req.Method)
			name := "demo"
			path := "/demo"
			g1 := &kc_model.GroupRepresentation{
				ID:        &id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id) {
			assert.Equal(t, "GET", req.Method)
			c1 := &kc_model.CredentialRepresentation{
				ID:    id.String(),
				Type:  "password",
				Value: "1234",
			}
			groups := []*kc_model.CredentialRepresentation{c1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		rw.Write([]byte{})
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	got := kc.NewClientWithAccessToken(token).Users.Delete(&id)
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Data)
}

func TestUserResourceGetInformation(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "GET", req.Method)
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			fmt.Sprintf("/admin/realms/demo/users/%s", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s", id) {
			username := "jdoe@demo.com"
			firstname := "John"
			lastname := "DOE"
			user := &kc_model.UserRepresentation{
				ID:        &id,
				Username:  &username,
				FirstName: &firstname,
				LastName:  &lastname,
			}
			response, err := json.Marshal(user)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			name := "demo"
			path := "/demo"
			g1 := &kc_model.GroupRepresentation{
				ID:        &id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id) {
			c1 := &kc_model.CredentialRepresentation{
				ID:    id.String(),
				Type:  "password",
				Value: "1234",
			}
			groups := []*kc_model.CredentialRepresentation{c1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		t.Fail()
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	got := kc.NewClientWithAccessToken(token).Users.GetInformation(&id)
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Data)
}

func TestUserResourceGetGroups(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "GET", req.Method)
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		assert.Equal(t, req.URL.String(), fmt.Sprintf("/admin/realms/demo/users/%s/groups", id))
		name := "demo"
		path := "/demo"
		g1 := &kc_model.GroupRepresentation{
			ID:        &id,
			Name:      &name,
			Path:      &path,
			SubGroups: nil,
		}
		groups := []*kc_model.GroupRepresentation{g1}
		response, err := json.Marshal(groups)
		assert.Nil(t, err)
		rw.Write(response)
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	got := kc.NewClientWithAccessToken(token).Users.GetGroups(&id)
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Data)
}

func TestUserResourceGetCredentials(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "GET", req.Method)
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		assert.Equal(t, req.URL.String(), fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id))
		c1 := &kc_model.CredentialRepresentation{
			ID:    id.String(),
			Type:  "password",
			Value: "1234",
		}
		groups := []*kc_model.CredentialRepresentation{c1}
		response, err := json.Marshal(groups)
		assert.Nil(t, err)
		rw.Write(response)
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	got := kc.NewClientWithAccessToken(token).Users.GetCredentials(&id)
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Data)
}

func TestUserResourceEnable(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			fmt.Sprintf("/admin/realms/demo/users/%s", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s", id) {
			if req.Method == "PUT" {
				req_body, err := ioutil.ReadAll(req.Body)
				if err != nil {
					t.Fatal(err)
				}
				var data *kc_model.UserRepresentation
				err = json.Unmarshal(req_body, &data)
				assert.Nil(t, err)
				assert.NotNil(t, data)
				assert.Equal(t, true, *data.Enabled)
				rw.Write([]byte{})
				return
			}
			if req.Method == "GET" {
				username := "jdoe@demo.com"
				firstname := "John"
				lastname := "DOE"
				user := &kc_model.UserRepresentation{
					ID:        &id,
					Username:  &username,
					FirstName: &firstname,
					LastName:  &lastname,
				}
				response, err := json.Marshal(user)
				assert.Nil(t, err)
				rw.Write(response)
				return
			}
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			assert.Equal(t, "GET", req.Method)
			name := "demo"
			path := "/demo"
			g1 := &kc_model.GroupRepresentation{
				ID:        &id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id) {
			assert.Equal(t, "GET", req.Method)
			c1 := &kc_model.CredentialRepresentation{
				ID:    id.String(),
				Type:  "password",
				Value: "1234",
			}
			groups := []*kc_model.CredentialRepresentation{c1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		t.Fail()
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	got := kc.NewClientWithAccessToken(token).Users.Enable(&id, true)
	if got.Error != nil {
		t.Logf("error : %v\n", got.Error.Error())
	}
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Data)
}

func TestUserResourceLeaveGroup(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	group_id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			assert.Equal(t, "GET", req.Method)
			name := "demo_group"
			path := "/demo_group"
			g1 := &kc_model.GroupRepresentation{
				ID:        &group_id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id) {
			assert.Equal(t, "DELETE", req.Method)
			assert.Equal(t, req.URL.String(), fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id))
			rw.Write([]byte{})
		}
		rw.Write([]byte{})
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	group := "demo_group"
	got := kc.NewClientWithAccessToken(token).Users.LeaveGroup(&id, group)
	assert.Nil(t, got.Error)
	assert.Equal(t, http.StatusOK, got.StatusCode)
	assert.Nil(t, got.Data)
}

/*
func TestUserResourceLeaveGroupGroupNotFound(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	group_id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			assert.Equal(t, "GET", req.Method)
			name := "demo_group"
			path := "/demo_group"
			g1 := &kc_model.GroupRepresentation{
				ID:        &group_id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id) {
			assert.Equal(t, "DELETE", req.Method)
			assert.Equal(t, req.URL.String(), fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id))
			rw.Write([]byte{})
		}
		rw.Write([]byte{})
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	group := "not_found"
	got := kc.NewClientWithAccessToken(token).Users.LeaveGroup(&id, group)
	assert.NotNil(t, got.Error)
	assert.Equal(t, http.StatusNotFound, got.StatusCode)
}
*/

func TestUserResourceJoinGroup(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	group_id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			"/admin/realms/demo/groups",
			fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == "/admin/realms/demo/groups" {
			assert.Equal(t, "GET", req.Method)
			name := "demo_group"
			path := "/demo_group"
			g1 := &kc_model.GroupRepresentation{
				ID:        &group_id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id) {
			assert.Equal(t, "PUT", req.Method)
			assert.Equal(t, req.URL.String(), fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id))
			rw.Write([]byte{})
			return
		}
		t.Fail()
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	group := "demo_group"
	got := kc.NewClientWithAccessToken(token).Users.JoinGroup(&id, group)
	assert.Nil(t, got.Error)
	assert.Equal(t, http.StatusOK, got.StatusCode)
	assert.Nil(t, got.Data)
}

/*
func TestUserResourceJoinGroupGroupNotFound(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	group_id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			"/admin/realms/demo/groups",
			fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == "/admin/realms/demo/groups" {
			assert.Equal(t, "GET", req.Method)
			name := "demo_group"
			path := "/demo_group"
			g1 := &kc_model.GroupRepresentation{
				ID:        &id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id) {
			assert.Equal(t, "PUT", req.Method)
			assert.Equal(t, req.URL.String(), fmt.Sprintf("/admin/realms/demo/users/%s/groups/%s", id, group_id))
			rw.Write([]byte{})
			return
		}
		t.Fail()
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	group := "not_found"
	got := kc.NewClientWithAccessToken(token).Users.JoinGroup(&id, group)
	assert.NotNil(t, got.Error)
	assert.Equal(t, http.StatusNotFound, got.StatusCode)
	assert.Nil(t, got.Data)
}
*/

func TestUserResourceGetList(t *testing.T) {
	id, err := uuid.NewUUID()
	assert.Nil(t, err)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, "Bearer new_access_token", req.Header.Get("Authorization"))
		allowed_path := []string{
			"/admin/realms/demo/users",
			fmt.Sprintf("/admin/realms/demo/users/%s/groups", id),
			fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id),
		}
		assert.Contains(t, allowed_path, req.URL.String())
		if req.URL.String() == "/admin/realms/demo/users" {
			assert.Equal(t, "GET", req.Method)
			username := "jdoe@demo.com"
			firstname := "John"
			lastname := "DOE"
			u1 := &kc_model.UserRepresentation{
				ID:        &id,
				Username:  &username,
				FirstName: &firstname,
				LastName:  &lastname,
			}
			users := []*kc_model.UserRepresentation{u1}
			response, err := json.Marshal(users)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/groups", id) {
			name := "demo"
			path := "/demo"
			g1 := &kc_model.GroupRepresentation{
				ID:        &id,
				Name:      &name,
				Path:      &path,
				SubGroups: nil,
			}
			groups := []*kc_model.GroupRepresentation{g1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		if req.URL.String() == fmt.Sprintf("/admin/realms/demo/users/%s/credentials", id) {
			c1 := &kc_model.CredentialRepresentation{
				ID:    id.String(),
				Type:  "password",
				Value: "1234",
			}
			groups := []*kc_model.CredentialRepresentation{c1}
			response, err := json.Marshal(groups)
			assert.Nil(t, err)
			rw.Write(response)
			return
		}
		t.Fail()
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)
	token := "new_access_token"

	got := kc.NewClientWithAccessToken(token).Users.GetList(nil)
	assert.Nil(t, got.Error)
	assert.NotNil(t, got.Data)
}
