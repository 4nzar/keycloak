package test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	kc "gitlab.com/4nzar/keycloak/pkg/clients"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func TestNewClient(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, "application/x-www-form-urlencoded", req.Header.Get("Content-Type"))
		assert.Equal(t, req.URL.String(), "/realms/demo/protocol/openid-connect/token")
		req.ParseForm()
		assert.Equal(t, "jdoe@demo.com", req.PostFormValue("username"))
		assert.Equal(t, "1234", req.PostFormValue("password"))
		token := kc_model.AuthToken{
			AccessToken:      "new_access_token",
			ExpiresIn:        60,
			RefreshExpiresIn: 1800,
			RefreshToken:     "new_refresh_token",
			TokenType:        "bearer",
			NotBeforePolicy:  0,
			SessionState:     "new_session_state",
			Scope:            "profile email",
		}
		data, err := json.Marshal(&token)
		if err != nil {
			t.Logf("error on Marshal : %v\n", err)
			t.Fail()
		}
		rw.Write(data)
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)

	username := "jdoe@demo.com"
	password := "1234"
	got := kc.NewClient(&username, &password)
	assert.NotNil(t, got)
	assert.NotNil(t, got.Username)
	assert.NotNil(t, got.Password)
	assert.NotNil(t, got.Token)
	assert.True(t, got.CanBeAuthenticated())
}

func TestNewClientShouldFail(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, "application/x-www-form-urlencoded", req.Header.Get("Content-Type"))
		assert.Equal(t, req.URL.String(), "/realms/demo/protocol/openid-connect/token")
		req.ParseForm()
		assert.Equal(t, "jdoe@demo.com", req.PostFormValue("username"))
		assert.Equal(t, "1234", req.PostFormValue("password"))
		token := kc_model.AuthToken{
			AccessToken:      "new_access_token",
			ExpiresIn:        60,
			RefreshExpiresIn: 1800,
			RefreshToken:     "new_refresh_token",
			TokenType:        "bearer",
			NotBeforePolicy:  0,
			SessionState:     "new_session_state",
			Scope:            "profile email",
		}
		data, err := json.Marshal(&token)
		if err != nil {
			t.Logf("error on Marshal : %v\n", err)
			t.Fail()
		}
		rw.Write(data)
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)

	username := "jdoe@demo.com"
	got := kc.NewClient(&username, nil)
	assert.Nil(t, got)
}

func TestNewClientWithToken(t *testing.T) {
	token := &kc_model.AuthToken{
		AccessToken: "new_access_token",
	}
	got := kc.NewClientWithToken(token)
	assert.NotNil(t, got)
	assert.Nil(t, got.Username)
	assert.Nil(t, got.Password)
	assert.NotNil(t, got.Token)
	assert.True(t, got.CanBeAuthenticated())
}

func TestNewClientWithAccessToken(t *testing.T) {
	access_token := "new_access_token"
	got := kc.NewClientWithAccessToken(access_token)
	assert.NotNil(t, got)
	assert.Nil(t, got.Username)
	assert.Nil(t, got.Password)
	assert.NotNil(t, got.Token)
	assert.True(t, got.CanBeAuthenticated())
}
