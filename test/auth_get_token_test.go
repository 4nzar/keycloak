package test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	kc_auth "gitlab.com/4nzar/keycloak/pkg/auth"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func TestGetTokenWithDefaultValueOnNewVersion(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, "application/x-www-form-urlencoded", req.Header.Get("Content-Type"))
		assert.Equal(t, req.URL.String(), "/realms/demo/protocol/openid-connect/token")
		req.ParseForm()
		assert.Equal(t, "Jdoe", req.PostFormValue("username"))
		assert.Equal(t, "1234", req.PostFormValue("password"))
		token := kc_model.AuthToken{
			AccessToken:      "new_access_token",
			ExpiresIn:        60,
			RefreshExpiresIn: 1800,
			RefreshToken:     "new_refresh_token",
			TokenType:        "bearer",
			NotBeforePolicy:  0,
			SessionState:     "new_session_state",
			Scope:            "profile email",
		}
		data, err := json.Marshal(&token)
		if err != nil {
			t.Logf("error on Marshal : %v\n", err)
			t.Fail()
		}
		rw.Write(data)
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)

	auth_info := kc_model.AuthInfo{
		Username: "Jdoe",
		Password: "1234",
	}

	got, err := kc_auth.GetToken(auth_info)
	assert.Nil(t, err)
	if err != nil {
		t.Logf("error : %v\n", err.Error())
	}
	assert.NotNil(t, got)
	assert.Equal(t, "new_access_token", *&got.AccessToken)
	assert.Equal(t, int64(60), *&got.ExpiresIn)
	assert.Equal(t, int64(1800), *&got.RefreshExpiresIn)
	assert.Equal(t, "new_refresh_token", *&got.RefreshToken)
	assert.Equal(t, "bearer", *&got.TokenType)
	assert.Equal(t, int64(0), *&got.NotBeforePolicy)
	assert.Equal(t, "new_session_state", *&got.SessionState)
	assert.Equal(t, "profile email", *&got.Scope)
}

func TestGetTokenWithDefaultValueShouldRefresh(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "POST", req.Method)
		assert.Equal(t, "application/x-www-form-urlencoded", req.Header.Get("Content-Type"))
		assert.Equal(t, req.URL.String(), "/realms/demo/protocol/openid-connect/token")
		req.ParseForm()
		assert.Equal(t, "Jdoe", req.PostFormValue("username"))
		assert.Equal(t, "1234", req.PostFormValue("password"))
		token := kc_model.AuthToken{
			AccessToken:      "new_access_token",
			ExpiresIn:        -60,
			RefreshExpiresIn: -1800,
			RefreshToken:     "new_refresh_token",
			TokenType:        "bearer",
			NotBeforePolicy:  0,
			SessionState:     "new_session_state",
			Scope:            "profile email",
		}
		data, err := json.Marshal(&token)
		if err != nil {
			t.Logf("error on Marshal : %v\n", err)
			t.Fail()
		}
		rw.Write(data)
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)

	auth_info := kc_model.AuthInfo{
		Username: "Jdoe",
		Password: "1234",
	}

	_, err := kc_auth.GetToken(auth_info)
	assert.Nil(t, err)
	if err != nil {
		t.Logf("error : %v\n", err.Error())
	}

	got, err := kc_auth.GetToken(auth_info)
	assert.Nil(t, err)
	if err != nil {
		t.Logf("error : %v\n", err.Error())
	}
	assert.NotNil(t, got)
	assert.Equal(t, "new_access_token", *&got.AccessToken)
	// assert.Equal(t, int64(60), *&got.ExpiresIn)
	// assert.Equal(t, int64(1800), *&got.RefreshExpiresIn)
	assert.Equal(t, "new_refresh_token", *&got.RefreshToken)
	assert.Equal(t, "bearer", *&got.TokenType)
	assert.Equal(t, int64(0), *&got.NotBeforePolicy)
	assert.Equal(t, "new_session_state", *&got.SessionState)
	assert.Equal(t, "profile email", *&got.Scope)
}
