package test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	kc_auth "gitlab.com/4nzar/keycloak/pkg/auth"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func TestIdentifyOnNewVersion(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		t.Logf("URL : %s\n", req.URL.String())
		assert.Equal(t, "GET", req.Method)
		assert.Equal(t, "application/json; charset=UTF-8", req.Header.Get("Content-Type"))
		assert.Equal(t, req.URL.String(), "/realms/demo/protocol/openid-connect/userinfo")

		assert.Equal(t, "Bearer new_token", req.Header.Get("Authorization"))

		id, err := uuid.NewUUID()
		assert.Nil(t, err)
		verified := true
		name := "John"
		family_name := "DOE"
		email := "jdoe@demo.com"
		empty := ""
		token := kc_model.UserInformation{
			ID:                &id,
			EmailVerified:     &verified,
			Name:              &name,
			FamilyName:        &family_name,
			Email:             &email,
			PreferredUsername: &empty,
			GivenName:         &empty,
		}
		data, err := json.Marshal(&token)
		assert.Nil(t, err)
		if err != nil {
			t.Logf("error on Marshal : %v\n", err.Error())
			t.Fail()
		}
		rw.Write(data)
	}))
	defer server.Close()

	t.Setenv("KC_URL", server.URL)

	token := "new_token"
	got, err := kc_auth.Identify(token)
	assert.Nil(t, err)
	if err != nil {
		t.Logf("error : %v\n", err.Error())
	}
	assert.NotNil(t, got)
}
