package utils

import (
	"fmt"
	"log"
	"os"
)

func prepare() (string, string) {
	url, ok := os.LookupEnv("KC_URL")
	if !ok {
		url = "http://localhost:8080"
	}
	realm, ok := os.LookupEnv("KC_PLATFORM_REALM")
	if !ok {
		realm = "demo"
	}
	return url, realm
}

func GetFmtURL(admin bool, route string) string {
	log.Printf("GetFmtURL - admin: %v - route: %s", admin, route)
	url, realm := prepare()
	is_old, ok := os.LookupEnv("KC_OLD_VERSION")
	if !ok {
		is_old = "0"
	}
	if is_old != "1" {
		is_old = "0"
	}
	result := fmt.Sprintf("%s/realms/%s/%s", url, realm, route)
	if admin {
		result = fmt.Sprintf("%s/admin/realms/%s/%s", url, realm, route)
	}
	if is_old == "1" {
		result = fmt.Sprintf("%s/auth/realms/%s/%s", url, realm, route)
		if admin {
			result = fmt.Sprintf("%s/auth/admin/realms/%s/%s", url, realm, route)
		}
	}
	log.Printf("GetFmtURL - generated url: %s", result)
	return result
}
