package auth

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	goose_http "gitlab.com/4nzar/goose/pkg/http/form"
	"gitlab.com/4nzar/keycloak/internal/utils"
	"gitlab.com/4nzar/keycloak/pkg/model"
)

func DoAuthRequest(form url.Values, token kc_model.AuthToken) (kc_model.AuthToken, error) {
	path := utils.GetFmtURL(false, "protocol/openid-connect/token")
	headers := map[string]string{}
	queries := url.Values{}
	response, err := goose_http.PostForm(path, headers, queries, form)
	if err != nil || response.StatusCode >= http.StatusInternalServerError {
		return kc_model.AuthToken{}, err
	}
	data, err := io.ReadAll(response.Body)
	if response.StatusCode != http.StatusOK {
		return kc_model.AuthToken{}, fmt.Errorf(string(data))
	}
	if err != nil {
		return kc_model.AuthToken{}, err
	}
	if err := json.Unmarshal(data, &token); err != nil {
		return kc_model.AuthToken{}, err
	}
	token.ExpiresDate = time.Now().Add(time.Duration(token.ExpiresIn))
	token.RefreshExpiresDate = time.Now().Add(time.Duration(token.RefreshExpiresIn))
	return token, nil
}
