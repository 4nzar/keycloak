package auth

import (
	"os"
	"strings"
)

// Return client ID and client secret if it exists in environment variables
// else return default value
func getClientInformation() (string, string) {
	client_secret, ok := os.LookupEnv("KC_PLATFORM_CLIENT_SECRET")
	if !ok {
		client_secret = ""
	}
	client_secret = strings.TrimSpace(client_secret)
	client_id, ok := os.LookupEnv("KC_PLATFORM_CLIENT_ID")
	if !ok {
		client_id = "platform"
	}
	return client_id, client_secret
}
