package auth

import (
	"net/url"

	"gitlab.com/4nzar/keycloak/pkg/model"
)

func PrepareRequest(auth_info kc_model.AuthInfo, refresh_token *string) url.Values {
	form := url.Values{}
	client_id, client_secret := getClientInformation()
	form.Set("client_id", client_id)
	if len(client_secret) > 0 {
		form.Set("client_secret", client_secret)
	}
	if refresh_token != nil {
		form.Set("grant_type", "refresh_token")
		form.Set("refresh_token", *refresh_token)
		return form
	}
	form.Set("grant_type", "password")
	form.Set("username", auth_info.Username)
	form.Set("password", auth_info.Password)
	return form
}

func PrepareForm(username, password string, refresh_token *string) url.Values {
	form := url.Values{}
	client_id, client_secret := getClientInformation()
	form.Set("client_id", client_id)
	if len(client_secret) > 0 {
		form.Set("client_secret", client_secret)
	}
	if refresh_token != nil {
		form.Set("grant_type", "refresh_token")
		form.Set("refresh_token", *refresh_token)
		return form
	}
	form.Set("grant_type", "password")
	form.Set("username", username)
	form.Set("password", password)
	return form
}
